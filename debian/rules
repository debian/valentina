#!/usr/bin/make -f

# execute test
# * link with freshly built libraries
# * execute in virtual X11 environment
_TEST = LD_LIBRARY_PATH=$(subst $() $(),:,$(strip \
 $(patsubst %,$(CURDIR)/src/libs/%/bin,qmuparser vpropertyexplorer))) \
 QT_QPA_PLATFORM=offscreen \
 HOME=$(CURDIR)/debian/build \
 XDG_RUNTIME_DIR=$(CURDIR)/debian/tmp-xdg-runtime-dir \
 dbus-run-session -- \
 $(MAKE) -C src/test$(patsubst %,/%,$1) check

QMAKE_CONFIG = noStripDebugSymbols no_ccache release
ifneq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
QMAKE_CONFIG += noTest
endif

override_dh_auto_configure:
	mkdir --mode 0700 --parents debian/tmp-xdg-runtime-dir
# set Qt API
# install private shared libraries as such
# fix rpath to point to private shared libraries
	QT_SELECT=5 dh_auto_configure -- \
		PREFIX_LIB=/usr/lib/valentina \
		QMAKE_LFLAGS_RELEASE+=-Wl,-rpath,/usr/lib/valentina \
		QMAKE_LFLAGS_DEBUG+=-Wl,-rpath,/usr/lib/valentina \
		$(patsubst %,CONFIG+=%,$(QMAKE_CONFIG))

override_dh_auto_test:
ifneq (,$(filter armel armhf mips64el mipsel riscv64,$(DEB_HOST_ARCH)))
	$(call _TEST,CollectionTest) || true
else
	$(call _TEST,CollectionTest)
endif
	$(call _TEST,ParserTest)
ifneq (,$(filter i386 hurd-i386,$(DEB_HOST_ARCH)))
	$(call _TEST,ValentinaTest) || true
else
	$(call _TEST,ValentinaTest)
endif

override_dh_auto_test-indep:
	$(call _TEST,TranslationsTest)

# avoid suspected race condition (see bug#1070900)
override_dh_auto_install:
	dh_auto_install --no-parallel

# rename puzzle -> valentina-puzzle
# rename tape -> valentina-tape
# normalize paths embedded in example files
execute_after_dh_install-arch:
	chrpath --replace /usr/lib/valentina \
		debian/valentina/usr/bin/valentina \
		debian/valentina/usr/bin/puzzle \
		debian/valentina/usr/bin/tape
	mv debian/valentina/usr/bin/puzzle \
		debian/valentina/usr/bin/valentina-puzzle
	mv debian/valentina/usr/bin/tape \
		debian/valentina/usr/bin/valentina-tape
	mv debian/valentina/usr/share/applications/ua.com.smart-pattern.puzzle.desktop \
		debian/valentina/usr/share/applications/ua.com.smart-pattern.valentina-puzzle.desktop
	mv debian/valentina/usr/share/applications/ua.com.smart-pattern.tape.desktop \
		debian/valentina/usr/share/applications/ua.com.smart-pattern.valentina-tape.desktop
	perl -i -pe 's/(?:Exec|Icon)=\Kpuzzle/valentina-puzzle/' \
		debian/valentina/usr/share/applications/ua.com.smart-pattern.valentina-puzzle.desktop
	perl -i -pe 's/(?:Exec|Icon)=\Ktape/valentina-tape/' \
		debian/valentina/usr/share/applications/ua.com.smart-pattern.valentina-tape.desktop
	cp --recursive src/app/share/collection debian/collection
	find debian/collection -name '*.val' -exec perl -i -p \
		-e 's,<measurements>\K.*/src/app/valentina/bin,/usr/share/valentina,;' \
		-e 's,<measurements>/usr/share/valentina/tables/\Kstandard,multisize,' \
		'{}' +
execute_after_dh_installman-arch:
	mv debian/valentina/usr/share/man/man1/puzzle.1 \
		debian/valentina/usr/share/man/man1/valentina-puzzle.1
	mv debian/valentina/usr/share/man/man1/tape.1 \
		debian/valentina/usr/share/man/man1/valentina-tape.1

# install select documentation with all binary packages
override_dh_installdocs:
	dh_installdocs -- AUTHORS.txt README.md

execute_before_dh_clean:
	rm -rf debian/tmp-xdg-runtime-dir

%:
	dh $@
