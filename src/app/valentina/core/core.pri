# ADD TO EACH PATH $$PWD VARIABLE!!!!!!
# This need for corect working file translations.pro

HEADERS += \
    $$PWD/vapplication.h \
    $$PWD/vformulaproperty.h \
    $$PWD/vformulapropertyeditor.h \
    $$PWD/vtooloptionspropertybrowser.h \
    $$PWD/vcmdexport.h \
    $$PWD/vvalentinashortcutmanager.h

SOURCES += \
    $$PWD/vapplication.cpp \
    $$PWD/vformulaproperty.cpp \
    $$PWD/vformulapropertyeditor.cpp \
    $$PWD/vtooloptionspropertybrowser.cpp \
    $$PWD/vcmdexport.cpp \
    $$PWD/vvalentinashortcutmanager.cpp
