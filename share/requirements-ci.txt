bottle==0.12.25
Brotli==1.1.0
certifi==2024.2.2
charset-normalizer==3.3.2
colorama==0.4.6
conan==1.63.0
distro==1.8.0
dropbox==11.36.2
fasteners==0.19
idna==3.7
inflate64==1.0.0
Jinja2==3.1.3
MarkupSafe==2.1.5
multivolumefile==0.2.3
node-semver==0.6.1
patch-ng==1.17.4
pluginbase==1.0.1
ply==3.11
psutil==5.9.8
py7zr==0.21.0
pybcj==1.0.2
pycryptodomex==3.20.0
Pygments==2.17.2
PyJWT==2.8.0
pyppmd==1.1.0
python-dateutil==2.9.0.post0
PyYAML==6.0.1
pyzstd==0.15.10
requests==2.31.0
six==1.16.0
stone==3.3.3
texttable==1.7.0
tqdm==4.66.2
urllib3==1.26.18
